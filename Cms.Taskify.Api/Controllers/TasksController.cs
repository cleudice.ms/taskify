using Cms.Taskify.Shared.Commands;
using Microsoft.AspNetCore.Mvc;

namespace Cms.Taskfy.Api;

[ApiController]
[Route("api/[controller]")]
public class TasksController : ControllerBase
{
    [HttpPost]
    public IActionResult CreateTask([FromBody] CreateTaskCommand command)
    {
        return CreatedAtAction("CreateTask", null);
    }
}
