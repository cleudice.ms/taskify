﻿namespace Cms.Taskify.Shared.Commands;

public class CreateTaskCommand
{
    public required string Title { get; set; }
    public string? Description { get; set; }
}
