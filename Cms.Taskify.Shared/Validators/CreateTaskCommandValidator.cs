using Cms.Taskify.Shared.Commands;
using FluentValidation;

namespace Cms.Taskify.Shared.Validators;

public class CreateTaskCommandValidator : AbstractValidator<CreateTaskCommand>
{
    public CreateTaskCommandValidator()
    {
        RuleFor(x => x.Title)
            .NotEmpty()
            .WithErrorCode("NotEmptyValidator");
    }
}
